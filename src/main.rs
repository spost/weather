use std::fs;

use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
struct Config {
    api_key: String,
    location: String,
}

fn get_config() -> Result<Config, toml::de::Error> {
    let default_config: Config = Config {
        api_key: "Go to https://openweathermap.org/api".to_string(),
        location: "zip here".to_string(),
    };
    let xdg_dirs = xdg::BaseDirectories::with_prefix("weather")
        .expect("Could not locate configuration directory. Are your XDG variables set?");
    let config_file_path = xdg_dirs
        .place_config_file("config.toml")
        .expect("Could not locate configuration file.");
    let config_file_string = match fs::read_to_string(&config_file_path) {
        Ok(c) => c,
        Err(error) => match error.kind() {
            std::io::ErrorKind::NotFound => {
                fs::write(&config_file_path, toml::to_string(&default_config).unwrap()).unwrap();
                eprintln!("Wrote default config file, go update it");
                std::process::exit(1)
            }
            _ => panic!("Can't deal, bro"),
        },
    };
    toml::from_str(&config_file_string)
}

#[derive(Deserialize, Debug)]
struct LocationResponse {
    lat: f32,
    lon: f32,
}

fn main() {
    if let Ok(config) = get_config() {
        let location = reqwest::blocking::get(format!(
            "http://api.openweathermap.org/geo/1.0/zip?zip={zip}&appid={api_key}",
            zip = config.location,
            api_key = config.api_key
        ))
        .unwrap()
        .json::<LocationResponse>()
        .expect("Could not parse OpenWeatherMap location response");

        println!(
            "{}",
            reqwest::blocking::get(format!(
                "https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={api_key}&units=imperial",
                lat = location.lat,
                lon = location.lon,
                api_key = config.api_key
        ))
            .unwrap()
            .text()
            .unwrap()
        )
    }
}
